FROM golang:alpine

WORKDIR /app

RUN apk update && \
    apk add ca-certificates && \
    rm -rf /var/cache/apk/* && \
    mkdir /usr/local/share/ca-certificates/extra && \
    update-ca-certificates && \
    apk add openssl && \
    openssl req -x509 -nodes -newkey rsa:2048 -keyout localhost.key -out localhost.crt -days 3650 -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=*"

COPY go.mod go.sum /app/

RUN go mod download

COPY ./ /app/

RUN go install ./... && mv $GOPATH/bin/planets /app

ENV MONGO_HOST=mongo-svc
ENV PLANETS_CERT_PATH=/app/localhost.crt 
ENV PLANETS_KEY_PATH=/app/localhost.key

EXPOSE 80
EXPOSE 443

CMD [ "./planets", "-https" ]