package server

import (
	"net/http"
	"testing"
	"time"

	"github.com/barbosaigor/rq"
)

const reqResponse = "Hey there"

func helloHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hey there"))
}

func TestListenAndServe(t *testing.T) {
	svr := New(4100, http.HandlerFunc(helloHandler))
	go func() {
		timeout := time.After(15 * time.Second)
		err := svr.ListenAndServe()
		for {
			select {
			case <-err:
				t.Errorf("Server crashes %v", err)
				return
			case <-timeout:
				t.Error("Server Timeout")
				return
			}
		}
	}()
	var res string
	requester := rq.Endpoint("localhost:4100").Get().ToString(&res)
	if err := requester.Err; err != nil {
		t.Errorf("Got an error trying to request server: %v", err)
		return
	}
	svr.Stop()
	if status := requester.StatusCode(); status != http.StatusOK {
		t.Errorf("Expected status code %v but got %v", http.StatusOK, status)
	}
	if res != reqResponse {
		t.Errorf("Expected %v but got %v", reqResponse, res)
	}
}
