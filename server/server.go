package server

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// Server hosts an API over HTTP
type Server struct {
	Port    int
	Handler http.Handler
	server  *http.Server
	ctx     context.Context
	// Stop pauses server gracefully
	Stop context.CancelFunc
}

// New creates a server instance
func New(port int, handler http.Handler) *Server {
	ctx, stop := context.WithCancel(context.Background())
	return &Server{
		Port:    port,
		Handler: handler,
		server:  nil,
		ctx:     ctx,
		Stop:    stop,
	}
}

// ListenAndServe hosts an HTTP server listening on Server.Port
func (s *Server) ListenAndServe() <-chan error {
	return s.startServer(s.Port, func(err chan error) {
		logrus.Infoln("(HTTP) Listening on port:", s.Port)
		err <- s.server.ListenAndServe()
	})
}

// ListenAndServeTLS hosts an HTTPS server listening on Server.HttpsPort.
// Always listens on port 443.
func (s *Server) ListenAndServeTLS(certPath, keyPath string) <-chan error {
	return s.startServer(443, func(err chan error) {
		logrus.Infoln("(HTTPS) Listening on port:", 443)
		err <- s.server.ListenAndServeTLS(certPath, keyPath)
	})
}

// startServer set up an http Server and hosts it.
// It allow clients to gracefully shut down the server.
func (s *Server) startServer(port int, run func(err chan error)) <-chan error {
	s.server = &http.Server{
		Handler:      s.Handler,
		Addr:         fmt.Sprintf("0.0.0.0:%d", port),
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	err := make(chan error)
	// Run server into separated go routine
	go run(err)
	// Listen for Stop signal to gracefully shutdown
	go func() {
		for {
			select {
			case <-s.ctx.Done():
				ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
				defer cancel()
				err <- s.server.Shutdown(ctx)
			}
		}
	}()
	return err
}
