package statefulrouter

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/barbosaigor/mondb"
	"gitlab.com/barbosaigor/planets/internal/planetmongo"
	"gitlab.com/barbosaigor/planets/planet"
)

var testPlanets = []planet.Planet{
	{
		Name:    "A",
		Climate: "Climate A",
		Terrain: "Terrain A",
		Appears: 21,
	},
	{
		Name:    "A",
		Climate: "Climate A2",
		Terrain: "Terrain A2",
		Appears: 700,
	},
	{
		Name:    "B",
		Climate: "Climate B",
		Terrain: "Terrain B",
		Appears: 80,
	},
	{
		Name:    "C",
		Climate: "Climate C",
		Terrain: "Terrain C",
		Appears: 2,
	},
	{
		Name:    "D",
		Climate: "Climate D",
		Terrain: "Terrain D",
		Appears: 7,
	},
}

type planetsResponse []planet.Planet

var monDB *planetmongo.PlanetMongo

func TestMain(m *testing.M) {
	monDB = planetmongo.New()
	if err := monDB.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		panic(fmt.Sprintf("Main: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err))
	}
	purgeTestData(monDB)
	code := m.Run()
	os.Exit(code)
}

func insertTestData(db *planetmongo.PlanetMongo) {
	for _, planet := range testPlanets {
		db.InsertPlanet(planet)
	}
}

func purgeTestData(db *planetmongo.PlanetMongo) {
	for _, planet := range testPlanets {
		for db.DeletePlanetByName(planet.Name) == nil {
		}
	}
}

func cmp(a, b []planet.Planet) bool {
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestGetPlanets(t *testing.T) {
	insertTestData(monDB)
	defer purgeTestData(monDB)
	srouter := New(monDB)
	// Get all planets
	{
		handler := srouter.getPlanets
		router := httprouter.New()
		router.GET("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodGet, "/planets", nil)
		if err != nil {
			t.Errorf("GetPlanets: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("GetPlanets: Expected status code %v but got %v", http.StatusOK, status)
		}
		body := recorder.Body.Bytes()
		var planetsJSON planetsResponse
		if err := json.Unmarshal(body, &planetsJSON); err != nil {
			t.Errorf("GetPlanets: Got an error when unmarshaling response body: %v", err)
			return
		}
		if len(testPlanets) != len(planetsJSON) {
			t.Errorf("GetPlanets: Expected %v elements but got %v", len(testPlanets), len(planetsJSON))
		}
		// ID is not important in this case, so we are removing them
		for i := range planetsJSON {
			planetsJSON[i].ID = ""
		}
		if !cmp(planetsJSON, testPlanets) {
			t.Errorf("GetPlanets: Wrong elements returned. Expected %v but got %v", testPlanets, planetsJSON)
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("GetPlanets: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}
	// Get planet by name
	{
		handler := srouter.getPlanets
		router := httprouter.New()
		router.GET("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodGet, "/planets?name=A", nil)
		if err != nil {
			t.Errorf("GetPlanets: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("GetPlanets: Expected status code %v but got %v", http.StatusOK, status)
		}
		body := recorder.Body.Bytes()
		var planetsJSON planetsResponse
		if err := json.Unmarshal(body, &planetsJSON); err != nil {
			t.Errorf("GetPlanets: Got an error when unmarshaling response body: %v", err)
			return
		}
		if len(planetsJSON) != 2 {
			t.Errorf("GetPlanets: Expected %v elements but got %v", 2, len(planetsJSON))
		}
		// ID is not important in this case, so we are removing them
		for i := range planetsJSON {
			planetsJSON[i].ID = ""
		}
		if !cmp(planetsJSON, testPlanets[:2]) {
			t.Errorf("GetPlanets: Wrong elements returned. Expected %v but got %v", testPlanets, planetsJSON[:2])
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("GetPlanets: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}
	// Get planet by invalid name
	{
		handler := srouter.getPlanets
		router := httprouter.New()
		router.GET("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodGet, "/planets?name=WrongName", nil)
		if err != nil {
			t.Errorf("GetPlanets: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusNotFound {
			t.Errorf("GetPlanets: Expected status code %v but got %v", http.StatusNotFound, status)
		}
		body := recorder.Body.Bytes()
		var planetsJSON planetsResponse
		if err := json.Unmarshal(body, &planetsJSON); err == nil {
			t.Error("GetPlanets: Expected an error while unmarshaling response body, but got nil")
			return
		}
	}
}

func TestGetPlanet(t *testing.T) {
	insertTestData(monDB)
	defer purgeTestData(monDB)
	srouter := New(monDB)
	// Get planet by an existing ID
	{
		id, err := getPlanetID(srouter, testPlanets[2])
		if err != nil {
			t.Errorf("GetPlanet: Got an error while getting a planet %v", err)
			return
		}
		handler := srouter.getPlanet
		router := httprouter.New()
		router.GET("/planets/:id", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodGet, "/planets/"+id, nil)
		if err != nil {
			t.Errorf("GetPlanet: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("GetPlanet: Expected status code %v but got %v", http.StatusOK, status)
		}
		body := recorder.Body.Bytes()
		var planetJSON planet.Planet
		if err := json.Unmarshal(body, &planetJSON); err != nil {
			t.Errorf("GetPlanet: Got an error when unmarshaling response body: %v", err)
			return
		}
		// ID is not important in this case, so we are removing it
		planetJSON.ID = ""
		if planetJSON != testPlanets[2] {
			t.Errorf("GetPlanet: Expected %v elements but got %v", planetJSON, testPlanets[2])
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("GetPlanet: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}

	// Get planet by wrong ID
	{
		handler := srouter.getPlanet
		router := httprouter.New()
		router.GET("/planets/:id", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodGet, "/planets/wrongid", nil)
		if err != nil {
			t.Errorf("GetPlanet: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusNotFound {
			t.Errorf("GetPlanet: Expected status code %v but got %v", http.StatusNotFound, status)
		}
	}
}

func TestInsertPlanet(t *testing.T) {
	insertTestData(monDB)
	defer purgeTestData(monDB)
	srouter := New(monDB)
	// Insert planet
	{
		earth := planet.Planet{
			Name:    "Earth",
			Climate: "Warm",
			Terrain: "mostly water",
		}
		reqEarth, err := json.Marshal(earth)
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while marshalling a planet: %v", err)
			return
		}
		handler := srouter.insertPlanet
		router := httprouter.New()
		router.POST("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPost, "/planets", bytes.NewBuffer(reqEarth))
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while creating a http.Request: %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("InsertPlanet: Expected status code %v but got %v", http.StatusOK, status)
		}
		id, err := getPlanetID(srouter, earth)
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while searching for a planet: %v", err)
			return
		}
		defer srouter.DB.DeletePlanet(id)
	}
	// Insert planet with missing Climate field
	{
		earth := planet.Planet{
			Name:    "Earth",
			Terrain: "mostly water",
		}
		reqEarth, err := json.Marshal(earth)
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while marshalling a planet: %v", err)
			return
		}
		handler := srouter.insertPlanet
		router := httprouter.New()
		router.POST("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPost, "/planets", bytes.NewBuffer(reqEarth))
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while creating a http.Request: %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusBadRequest {
			t.Errorf("InsertPlanet: Expected status code %v but got %v", http.StatusBadRequest, status)
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("InsertPlanet: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}
	// Insert planet with missing terrain field
	{
		earth := planet.Planet{
			Name:    "Earth",
			Climate: "Cold",
		}
		reqEarth, err := json.Marshal(earth)
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while marshalling a planet: %v", err)
			return
		}
		handler := srouter.insertPlanet
		router := httprouter.New()
		router.POST("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPost, "/planets", bytes.NewBuffer(reqEarth))
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while creating a http.Request: %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusBadRequest {
			t.Errorf("InsertPlanet: Expected status code %v but got %v", http.StatusBadRequest, status)
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("InsertPlanet: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}
	// Insert planet with missing climate and terrain field
	{
		earth := planet.Planet{
			Name: "Earth",
		}
		reqEarth, err := json.Marshal(earth)
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while marshalling a planet: %v", err)
			return
		}
		handler := srouter.insertPlanet
		router := httprouter.New()
		router.POST("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPost, "/planets", bytes.NewBuffer(reqEarth))
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while creating a http.Request: %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusBadRequest {
			t.Errorf("InsertPlanet: Expected status code %v but got %v", http.StatusBadRequest, status)
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("InsertPlanet: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}
	// Insert planet with missing name field
	{
		earth := planet.Planet{
			Climate: "Cold",
			Terrain: "Mostly water",
		}
		reqEarth, err := json.Marshal(earth)
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while marshalling a planet: %v", err)
			return
		}
		handler := srouter.insertPlanet
		router := httprouter.New()
		router.POST("/planets", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPost, "/planets", bytes.NewBuffer(reqEarth))
		if err != nil {
			t.Errorf("InsertPlanet: Got an error while creating a http.Request: %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusBadRequest {
			t.Errorf("InsertPlanet: Expected status code %v but got %v", http.StatusBadRequest, status)
		}
		expectedContentType := "application/json"
		if contentType := recorder.Header().Get("Content-Type"); contentType != expectedContentType {
			t.Errorf("InsertPlanet: Expected content type %v but got %v", expectedContentType, expectedContentType)
		}
	}
}

func TestDeletePlanet(t *testing.T) {
	insertTestData(monDB)
	defer purgeTestData(monDB)
	srouter := New(monDB)
	// Delete planet by an existing ID
	{
		id, err := getPlanetID(srouter, testPlanets[2])
		if err != nil {
			t.Errorf("DeletePlanet: Got an error while getting a planet %v", err)
			return
		}
		handler := srouter.deletePlanet
		router := httprouter.New()
		router.DELETE("/planets/:id", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodDelete, "/planets/"+id, nil)
		if err != nil {
			t.Errorf("DeletePlanet: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("DeletePlanet: Expected status code %v but got %v", http.StatusOK, status)
		}
	}

	// Delete planet by wrong ID
	{
		handler := srouter.deletePlanet
		router := httprouter.New()
		router.DELETE("/planets/:id", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodDelete, "/planets/wrongid", nil)
		if err != nil {
			t.Errorf("DeletePlanet: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusNotFound {
			t.Errorf("DeletePlanet: Expected status code %v but got %v", http.StatusNotFound, status)
		}
	}
}

func TestUpdatePlanetAppears(t *testing.T) {
	insertTestData(monDB)
	defer purgeTestData(monDB)
	srouter := New(monDB)
	planetAppears := struct {
		Appears int `json:"appears"`
	}{88}
	reqTestPlante, err := json.Marshal(planetAppears)
	// Update planet by an existing ID and valid body
	{
		if err != nil {
			t.Errorf("UpdatePlanetAppears: Got an error while marshalling a appears: %v", err)
			return
		}
		id, err := getPlanetID(srouter, testPlanets[3])
		if err != nil {
			t.Errorf("UpdatePlanetAppears: Got an error while getting a planet %v", err)
			return
		}
		handler := srouter.updatePlanetAppears
		router := httprouter.New()
		router.PUT("/planets/:id", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPut, "/planets/"+id, bytes.NewBuffer(reqTestPlante))
		if err != nil {
			t.Errorf("UpdatePlanetAppears: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("UpdatePlanetAppears: Expected status code %v but got %v", http.StatusOK, status)
		}
	}

	// Update planet by wrong ID
	{
		handler := srouter.updatePlanetAppears
		router := httprouter.New()
		router.PUT("/planets/:id", handler)
		recorder := httptest.NewRecorder()
		req, err := http.NewRequest(http.MethodPut, "/planets/wrongid", bytes.NewBuffer(reqTestPlante))
		if err != nil {
			t.Errorf("UpdatePlanetAppears: Got an error while creating a http.Request %v", err)
			return
		}
		router.ServeHTTP(recorder, req)

		if status := recorder.Code; status != http.StatusNotFound {
			t.Errorf("UpdatePlanetAppears: Expected status code %v but got %v", http.StatusNotFound, status)
		}
	}
}

func TestOptionsRouter(t *testing.T) {
	handler := http.HandlerFunc(optionsRouter)
	recorder := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodOptions, "/planets", nil)
	if err != nil {
		t.Errorf("OptionsRouter: Got an error while creating a http.Request %v", err)
		return
	}
	req.Header.Set("Access-Control-Request-Method", "GET")
	handler.ServeHTTP(recorder, req)

	if status := recorder.Code; status != http.StatusNoContent {
		t.Errorf("OptionsRouter: Expected status code %v but got %v", http.StatusNoContent, status)
	}

	expectedCors := "*"
	if cors := recorder.Header().Get("Access-Control-Allow-Origin"); cors != expectedCors {
		t.Errorf("OptionsRouter: Expected cors %s but got %s", expectedCors, cors)
	}

	expectedMethods := "DELETE, GET, OPTIONS, POST"
	if corsMethods := recorder.Header().Get("Access-Control-Allow-Methods"); corsMethods != expectedMethods {
		t.Errorf("OptionsRouter: Expected cors methods %s but got %s", expectedMethods, corsMethods)
	}
}

func getPlanetID(srouter *StatefulRouter, p planet.Planet) (string, error) {
	planets, err := monDB.GetPlanetsByName(p.Name)
	if err != nil {
		return "", err
	}
	for _, pln := range planets {
		p.ID = pln.ID
		if pln == p {
			return pln.ID, nil
		}
	}
	return "", errors.New("Planet not found")
}
