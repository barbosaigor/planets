package statefulrouter

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/barbosaigor/planets/planetdb"
)

// StatefulRouter wraps a database, which is used
// by all defined routes.
type StatefulRouter struct {
	DB     planetdb.PlanetDB
	Router *httprouter.Router
}

// New creates a StatefulRouter using an db
func New(db planetdb.PlanetDB) *StatefulRouter {
	sr := &StatefulRouter{
		DB:     db,
		Router: httprouter.New(),
	}
	populateRoutes(sr)
	return sr
}

func populateRoutes(sr *StatefulRouter) {
	sr.Router.GlobalOPTIONS = http.HandlerFunc(optionsRouter)
	sr.Router.GET("/planets/:id", sr.getPlanet)
	sr.Router.GET("/planets", sr.getPlanets)
	sr.Router.POST("/planets", sr.insertPlanet)
	sr.Router.DELETE("/planets/:id", sr.deletePlanet)
	sr.Router.PUT("/planets/:id", sr.updatePlanetAppears)
}
