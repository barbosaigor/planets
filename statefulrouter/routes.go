package statefulrouter

import (
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/barbosaigor/planets/planet"
	"gitlab.com/barbosaigor/planets/planetdb"
	"gitlab.com/barbosaigor/planets/swapi"
)

type responseMessage struct {
	Message string `json:"message"`
}

// getPlanets list all planets if no name were specified.
// If planet name is defined, then it searches for that planet.
func (sr *StatefulRouter) getPlanets(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	if name := r.FormValue("name"); name != "" {
		sr.getPlanetsByName(name, w, r)
		return
	}
	planets, err := sr.DB.GetPlanets()
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if planets == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	jsonPlanets, err := json.Marshal(planets)
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonPlanets)
}

func (sr *StatefulRouter) getPlanetsByName(name string, w http.ResponseWriter, r *http.Request) {
	planets, err := sr.DB.GetPlanetsByName(name)
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if planets == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	jsonPlanet, err := json.Marshal(planets)
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonPlanet)
}

func (sr *StatefulRouter) getPlanet(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	id := params.ByName("id")
	planet, err := sr.DB.GetPlanet(id)
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if planet == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	jsonPlanet, err := json.Marshal(planet)
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonPlanet)
}

func (sr *StatefulRouter) insertPlanet(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var pln planet.Planet
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&pln); err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if pln.Name == "" || pln.Climate == "" || pln.Terrain == "" {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		msg := responseMessage{Message: "There are missing fields."}
		res, err := json.Marshal(msg)
		if err != nil {
			logrus.Errorf("%v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(res)
		return
	}
	appears, err := swapi.GetPlanetApparitions(pln.Name)
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	pln.Appears = appears
	if err := sr.DB.InsertPlanet(pln); err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (sr *StatefulRouter) deletePlanet(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	id := params.ByName("id")
	err := sr.DB.DeletePlanet(id)
	if err == planetdb.ErrPlanetNotFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (sr *StatefulRouter) updatePlanetAppears(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var appears struct {
		Appears *int `json:"appears"`
	}
	if err := json.NewDecoder(r.Body).Decode(&appears); err != nil || appears.Appears == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	id := params.ByName("id")
	err := sr.DB.UpdatePlanet(id, planet.Planet{Appears: *appears.Appears})
	if err == planetdb.ErrPlanetNotFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		logrus.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func optionsRouter(w http.ResponseWriter, r *http.Request) {
	// Set CORS headers
	if r.Header.Get("Access-Control-Request-Method") != "" {
		w.Header().Set("Access-Control-Allow-Methods", "DELETE, GET, OPTIONS, POST")
		w.Header().Set("Access-Control-Allow-Origin", "*")
	}
	w.WriteHeader(http.StatusNoContent)
}
