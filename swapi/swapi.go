package swapi

import (
	"errors"
	"net/url"

	"github.com/barbosaigor/rq"
	"github.com/sirupsen/logrus"
)

var (
	// ErrSWAPI defines an error when the SWAPI doesn't work correctly
	ErrSWAPI = errors.New("Fail to fetch SWAPI")
	// ErrNotFound defines an error when the SWAPI doesn't work correctly
	ErrNotFound = errors.New("Resource Not Found")
)

// GetPlanetApparitions will count all movie apparitions of a planet.
func GetPlanetApparitions(name string) (int, error) {
	res, err := fetchPlanet(name)
	if err != nil {
		return -1, err
	}
	if res.Count == 0 {
		return 0, nil
	}
	for _, planet := range res.Results {
		if planet.Name == name {
			return len(planet.Films), nil
		}
	}
	return 0, nil
}

type swapiResJSON struct {
	Count   int `json:"count"`
	Results []struct {
		Name  string   `json:"name"`
		Films []string `json:"films"`
	} `json:"results"`
}

func fetchPlanet(name string) (*swapiResJSON, error) {
	var res swapiResJSON
	endpoint := "https://swapi.dev/api/planets/?search=" + url.QueryEscape(name)
	if err := rq.Endpoint(endpoint).Get().ToJSON(&res).Err; err != nil {
		logrus.Error(err)
		return nil, ErrSWAPI
	}
	return &res, nil
}
