package swapi

import "testing"

func TestGetPlanetApparitions(t *testing.T) {
	// Not existing planet
	apparitions, err := GetPlanetApparitions("NonSenseName")
	if err != nil {
		t.Errorf("GetPlanetApparitions: Got an error %v", err)
	}
	if apparitions != 0 {
		t.Errorf("GetPlanetApparitions: Expected 0 apparitions but got %v", apparitions)
	}

	// Existing planet
	apparitions, err = GetPlanetApparitions("Tatooine")
	if err != nil {
		t.Errorf("GetPlanetApparitions: Got an error %v", err)
	}
	if apparitions < 5 {
		t.Errorf("GetPlanetApparitions: Expected apparitions greater than 5 but got %v", apparitions)
	}
}
