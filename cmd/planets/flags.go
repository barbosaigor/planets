package main

import (
	"flag"
)

var httpPort int
var https bool
var databaseEndpoint string

func init() {
	flag.IntVar(&httpPort, "p", 80, "-p Port defines HTTP server port (default is 80)")
	flag.BoolVar(&https, "https", false, "-https Hosts an HTTPS server (listening on port 443)")
	flag.StringVar(&databaseEndpoint, "db", "", "-db Database defines a database endpoint")
}
