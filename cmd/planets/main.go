package main

import (
	"flag"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/barbosaigor/mondb"
	"gitlab.com/barbosaigor/planets/internal/planetmongo"
	"gitlab.com/barbosaigor/planets/server"
	"gitlab.com/barbosaigor/planets/statefulrouter"
)

func main() {
	flag.Parse()
	defineDatabaseEndpoint()
	// Creates a mongodb implementation
	db := planetmongo.New()
	if err := db.Conn("sw", "planets", databaseEndpoint); err != nil {
		panic(err)
	}
	// In order to allow routes to access the database, and
	// avoid a huge amount of open/close connections, a stateful router is handy.
	router := statefulrouter.New(db)
	if https {
		go func() {
			svr := server.New(httpPort, router.Router)
			logrus.Fatal(<-svr.ListenAndServeTLS(getCerts()))
		}()
	}
	svr := server.New(httpPort, router.Router)
	logrus.Fatal(<-svr.ListenAndServe())
}

func defineDatabaseEndpoint() {
	if databaseEndpoint == "" {
		if databaseEndpoint = os.Getenv("MONGO_HOST"); databaseEndpoint == "" {
			databaseEndpoint = mondb.DefaultMongoURL
		}
	}
	if !strings.HasPrefix(databaseEndpoint, "mongodb") {
		databaseEndpoint = "mongodb://" + databaseEndpoint + ":27017"
	}
}

func getCerts() (string, string) {
	cert, key := os.Getenv("PLANETS_CERT_PATH"), os.Getenv("PLANETS_KEY_PATH")
	if cert == "" {
		panic("No cert path specified at PLANETS_CERT_PATH environment")
	}
	if key == "" {
		panic("No key path specified at PLANETS_KEY_PATH environment")
	}
	return cert, key
}
