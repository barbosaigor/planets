package planet

// Planet stores all informations about a planet
type Planet struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Climate string `json:"climate"`
	Terrain string `json:"terrain"`
	// Appears counts how many movies this planet appeared
	Appears int `json:"appears"`
}
