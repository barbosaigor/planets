# Planets
Planets is a microservice for Star Wars game. 
Planets implements an API, which uses REST over HTTP/HTTPS.  

## How to Use
For Planets to run normally, it needs to connect to mongodb. 
You can run this service either just running a Docker container 
```bash
$ docker run -p 80:80 -p 443:443 -e MONGO_HOST="your-mongodb-url" barbosaigor/planets:latest
```
Or installing and running with
```bash
$ go get -u gitlab.com/barbosaigor/planets/...
$ $GOBIN/planets
```

**Flags**  
-p Defines HTTP server port (default is 80)  
-https Hosts an HTTPS server (listening on port 443). For https server, you must provide the certificates paths: PLANETS_CERT_PATH, PLANETS_KEY_PATH as environment variable.  
e.g  PLANETS_CERT_PATH=/.../mycert.crt PLANETS_KEY_PATH=/.../mykey.key planets -https  
-db Database defines the database endpoint (default is mongodb://localhost:27017)  
You can set the database using flag -db or setting up MONGO_HOST environment.

## Testing
In order to test this service, you should have a mongodb running,
all tests will connect to mongodb://localhost:27017 though.  
An easy way to set up it, is to run a mongo container with 
```bash
$ docker run -p 27017:27017 mongo:latest
```
Then just run 
```bash
$ go test ./...
```

## Design
In order to offer a complete gameplay consistency, the ‘appears’ is stored into the database as well as all planets data, and times in times, the Planets Updater service updates all planets.

As we are using a third-party API (SWAPI), we don’t know what level of service availability of SWAPI. As long as, players of a party are going to request the API, and some cases a few players could not get the response, or it could take a long time, and the inconsistency arises up. To prevent that, we will store the 'appears' (films count) of all our planets within the database.

For the purpose of getting consistent gameplay among all players, we will use a fast and consistent database, MongoDB.
Latency is almost always the priority for games, with this in mind, the development aims to use a low footprint memory and fewer CPU cycles. The Planets Updater service comes to release the assignment of updating 'appears' out of Planets service, therefore Planets service can focus only on replying clients.

![System Design of Planets API](./res/planets-design.png)

This project employs the [Planets Updater](https://gitlab.com/barbosaigor/planets-updater) service, to update all planets Appears data.  
The infrastructure is implemented at [planets-arch](https://gitlab.com/barbosaigor/planets-arch).

## API

**Get all planets**  
Method: *GET*  
URL: /planets  

**Get all planets by name**  
Method: *GET*  
URL: /planets?name=planetname  

**Get a planet**  
Method: *GET*  
URL: /planets/:id  

**Add a planet**  
Method: *POST*  
URL: /planets  
Must send a json  
```json
{
    name: "planet-name",
    climate: "planet-climate",
    terrain: "planet-terrain"
}
```  

**Delete a planet**  
Method: *DELETE*  
URL: /planets/:id  

