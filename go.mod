module gitlab.com/barbosaigor/planets

go 1.14

require (
	github.com/barbosaigor/rq v1.1.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/barbosaigor/mondb v1.0.5
)
