package planetmongo

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/barbosaigor/mondb"
	"gitlab.com/barbosaigor/planets/planet"
)

var (
	dbPlanets []planet.Planet = []planet.Planet{
		{
			Name:    "A",
			Climate: "Climate A",
			Terrain: "Terrain A",
			Appears: 21,
		},
		{
			Name:    "B",
			Climate: "Climate B",
			Terrain: "Terrain B",
			Appears: 80,
		},
		{
			Name:    "C",
			Climate: "Climate C",
			Terrain: "Terrain C",
			Appears: 2,
		},
		{
			Name:    "D",
			Climate: "Climate D",
			Terrain: "Terrain D",
			Appears: 7,
		},
	}
)

func (db *PlanetMongo) insertTestData() {
	for _, planet := range dbPlanets {
		db.InsertPlanet(planet)
	}
}

func (db *PlanetMongo) purgeTestData() {
	for _, planet := range dbPlanets {
		db.DeletePlanetByName(planet.Name)
	}
}

func purgeDB(db *PlanetMongo) {
	// Deletes all contents from test database before testing
	for {
		if deleted, err := db.mongo.DeleteOne(nil); !deleted || err != nil {
			break
		}
	}
}

func TestMain(m *testing.M) {
	db := New()
	if err := db.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		panic(fmt.Sprintf("Main: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err))
	}
	purgeDB(db)
	code := m.Run()
	purgeDB(db)
	os.Exit(code)
}

func TestNew(t *testing.T) {
	db := New()
	if db == nil {
		t.Error("New: Expected an object but got nil")
	}
}

func TestGetPlanets(t *testing.T) {
	db := New()
	if err := db.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		t.Errorf("GetPlanets: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err)
		return
	}
	db.insertTestData()
	defer db.purgeTestData()
	planets, err := db.GetPlanets()
	t.Logf("GetPlanets: planets -> %v", planets)
	if err != nil {
		t.Errorf("GetPlanets: Got an error %v", err)
		return
	}
	if planets == nil {
		t.Error("GetPlanets: Got nil planets")
	}
	if len(planets) != len(dbPlanets) {
		t.Errorf("GetPlanets: Expected %d planets but got %d", len(dbPlanets), len(planets))
	}
}

func TestInsertPlanet(t *testing.T) {
	db := New()
	if err := db.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		t.Errorf("GetPlanets: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err)
		return
	}
	earth := planet.Planet{
		Name:    "earth",
		Climate: "Warm",
		Terrain: "Almost everything is water",
		Appears: -1,
	}
	if err := db.InsertPlanet(earth); err != nil {
		t.Errorf("InsertPlanet: Got an error %v", err)
	}
}

func TestDeletePlanet(t *testing.T) {
	db := New()
	if err := db.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		t.Errorf("DeletePlanet: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err)
		return
	}
	db.purgeTestData()
	earth := planet.Planet{
		Name:    "earth",
		Climate: "Warm",
		Terrain: "Almost everything is water",
		Appears: -1,
	}
	if err := db.InsertPlanet(earth); err != nil {
		t.Errorf("DeletePlanet: Got an error while inserting a document %v", err)
	}
	earths, err := db.GetPlanetsByName("earth")
	if err != nil {
		t.Errorf("DeletePlanet: Got an error trying to find planet by name %v", err)
		return
	}
	if err := db.DeletePlanet(earths[0].ID); err != nil {
		t.Errorf("DeletePlanet: Got an error %v", err)
	}
}

func TestDeletePlanetByName(t *testing.T) {
	db := New()
	if err := db.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		t.Errorf("DeletePlanetByName: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err)
		return
	}
	db.purgeTestData()
	earth := planet.Planet{
		Name:    "earth",
		Climate: "Warm",
		Terrain: "Almost everything is water",
		Appears: -1,
	}
	if err := db.InsertPlanet(earth); err != nil {
		t.Errorf("DeletePlanetByName: Got an error while inserting a document %v", err)
	}
	earths, err := db.GetPlanetsByName("earth")
	if err != nil {
		t.Errorf("DeletePlanetByName: Got an error trying to find planet by name %v", err)
		return
	}
	if err := db.DeletePlanetByName(earths[0].Name); err != nil {
		t.Errorf("DeletePlanetByName: Got an error %v", err)
	}
}

func TestUpdatePlanet(t *testing.T) {
	db := New()
	if err := db.Conn("planetdb-test", "planets", mondb.DefaultMongoURL); err != nil {
		t.Errorf("UpdatePlanet: Fail to connect to database, please make sure you used the right endpoint and the database is up.\n%v", err)
		return
	}
	earth := planet.Planet{
		Name:    "earth",
		Climate: "Warm",
		Terrain: "Almost everything is water",
		Appears: -1,
	}
	if err := db.InsertPlanet(earth); err != nil {
		t.Errorf("UpdatePlanet: Got an error trying inserting a document %v", err)
	}
	earth.Name = "New name"
	earth.Terrain = "New terrain"
	getEarthID := func() (string, error) {
		earths, err := db.GetPlanetsByName("earth")
		if err != nil {
			return "", fmt.Errorf("UpdatePlanet: Got an error trying to find planet by name %w", err)
		}
		if len(earths) <= 0 {
			return "", fmt.Errorf("UpdatePlanet: No planets found by name %w", err)
		}
		return earths[0].ID, nil
	}
	earthID, err := getEarthID()
	if err != nil {
		t.Error(err)
		return
	}
	if err := db.UpdatePlanet(earthID, earth); err != nil {
		t.Errorf("UpdatePlanet: Got an error trying inserting a document %v", err)
	} else {
		planet, err := db.GetPlanet(earthID)
		if err != nil {
			t.Errorf("UpdatePlanet: Got an error trying get a document %v", err)
		}
		// ID is not important in this case, then we just ignore it when comparing.
		planet.ID = earth.ID
		if *planet != earth {
			t.Errorf("UpdatePlanet: Not updating right, Expected %v but got %v", earth, *planet)
		}
	}
}
