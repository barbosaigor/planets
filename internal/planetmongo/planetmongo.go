package planetmongo

import (
	"gitlab.com/barbosaigor/mondb"
	"gitlab.com/barbosaigor/planets/planet"
	"gitlab.com/barbosaigor/planets/planetdb"
)

// PlanetMongo operates over mongoDB
type PlanetMongo struct {
	mongo mondb.DocDB
}

// New creates a PlanetDB instance
func New() *PlanetMongo {
	return &PlanetMongo{}
}

// Conn makes a connection to a mongo database instance
func (db *PlanetMongo) Conn(dbName, collection, url string) error {
	if url == "" {
		url = mondb.DefaultMongoURL
	}
	db.mongo = &mondb.DocumentDB{Name: dbName, Collection: collection}
	return db.mongo.Conn(url)
}

// GetPlanets returns all planets
func (db *PlanetMongo) GetPlanets() ([]planet.Planet, error) {
	return db.getPlanetsBy(map[string]interface{}{})
}

// GetPlanetsByName searches for all planet that matches same name
func (db *PlanetMongo) GetPlanetsByName(name string) ([]planet.Planet, error) {
	return db.getPlanetsBy(map[string]interface{}{"name": name})
}

// getPlanetsBy searches all planets that matches the query.
func (db *PlanetMongo) getPlanetsBy(query map[string]interface{}) ([]planet.Planet, error) {
	docs, err := db.mongo.FindMany(query)
	if err != nil {
		if err == mondb.ErrDocumentNotFound {
			return nil, nil
		}
		return nil, err
	}
	return createPlanets(docs), nil
}

// createPlanets Iterate all documents to populate the planets
func createPlanets(docs []map[string]interface{}) []planet.Planet {
	var planets []planet.Planet
	for _, doc := range docs {
		planets = append(planets, createPlanetFromDoc(doc))
	}
	return planets
}

// GetPlanet searches for a planet by id
func (db *PlanetMongo) GetPlanet(id string) (*planet.Planet, error) {
	return db.getPlanetBy("_id", id)
}

// getPlanetBy searches for a planet.
// It will query for param that matches a value.
func (db *PlanetMongo) getPlanetBy(param, value string) (*planet.Planet, error) {
	doc, err := db.mongo.FindOne(map[string]interface{}{param: value})
	if err != nil {
		if err == mondb.ErrDocumentNotFound || err == mondb.ErrInvalidID {
			return nil, nil
		}
		return nil, err
	}
	planet := createPlanetFromDoc(doc)
	return &planet, nil
}

// InsertPlanet adds a new planet to the database
func (db *PlanetMongo) InsertPlanet(planet planet.Planet) error {
	return db.mongo.InsertOne(createDocFromPlanet(planet))
}

// DeletePlanet removes a planet by identifier
func (db *PlanetMongo) DeletePlanet(id string) error {
	deleted, err := db.mongo.DeleteOne(map[string]interface{}{"_id": id})
	if !deleted || err == mondb.ErrInvalidID {
		return planetdb.ErrPlanetNotFound
	}
	return err
}

// DeletePlanetByName removes a planet by name
func (db *PlanetMongo) DeletePlanetByName(name string) error {
	deleted, err := db.mongo.DeleteOne(map[string]interface{}{"name": name})
	if !deleted || err == mondb.ErrInvalidID {
		return planetdb.ErrPlanetNotFound
	}
	return err
}

// UpdatePlanet updates a planet
func (db *PlanetMongo) UpdatePlanet(id string, planet planet.Planet) error {
	updated, err := db.mongo.UpdateOne(map[string]interface{}{"_id": id}, createDocFromPlanet(planet))
	if !updated || err == mondb.ErrInvalidID {
		return planetdb.ErrPlanetNotFound
	}
	return err
}

// createPlanetFromDoc creates a planet from a document
func createPlanetFromDoc(doc map[string]interface{}) planet.Planet {
	var planet planet.Planet
	if ID, ok := doc["_id"]; ok {
		planet.ID = ID.(string)
	}
	if name, ok := doc["name"]; ok {
		planet.Name = name.(string)
	}
	if climate, ok := doc["climate"]; ok {
		planet.Climate = climate.(string)
	}
	if terrain, ok := doc["terrain"]; ok {
		planet.Terrain = terrain.(string)
	}
	if appears, ok := doc["appears"]; ok {
		planet.Appears = int(appears.(int32))
	}
	return planet
}

// createDocFromPlanet creates a document from a planet
func createDocFromPlanet(planet planet.Planet) map[string]interface{} {
	doc := map[string]interface{}{}
	if planet.Name != "" {
		doc["name"] = planet.Name
	}
	if planet.Climate != "" {
		doc["climate"] = planet.Climate
	}
	if planet.Terrain != "" {
		doc["terrain"] = planet.Terrain
	}
	doc["appears"] = planet.Appears
	return doc
}
