package planetdb

import "gitlab.com/barbosaigor/planets/planet"

// PlanetDB defines all operations required
// to manipulate planets on database
type PlanetDB interface {
	// GetPlanets searches for all planets
	GetPlanets() ([]planet.Planet, error)
	// GetPlanet searches for a planet by identifier
	GetPlanet(id string) (*planet.Planet, error)
	// GetPlanetsByName searches for all planets that matches same name
	GetPlanetsByName(name string) ([]planet.Planet, error)
	// InsertPlanet creates a new planet and stores it
	InsertPlanet(planet planet.Planet) error
	// DeletePlanet removes a planet by identifier
	DeletePlanet(id string) error
	// DeletePlanet updates a planet which matches the identifier
	UpdatePlanet(id string, planet planet.Planet) error
}
