package planetdb

import "errors"

var (
	// ErrPlanetNotFound defines that the planet was not found
	ErrPlanetNotFound = errors.New("Planet not found")
)
